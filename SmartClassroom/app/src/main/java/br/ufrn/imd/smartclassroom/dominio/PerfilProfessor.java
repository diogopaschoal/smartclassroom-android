package br.ufrn.imd.smartclassroom.dominio;

import java.io.Serializable;

/**
 * Created by Francisco on 16/11/2015.
 */
public class PerfilProfessor implements Serializable{

    private int id;
    private String nome;
    private String temperatura;
    private String slides;

    public PerfilProfessor(){}

    public PerfilProfessor(int id, String nome){
        this.id = id;
        this.nome = nome;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(String temperatura) {
        this.temperatura = temperatura;
    }

    public String getSlides() {
        return slides;
    }

    public void setSlides(String slides) {
        this.slides = slides;
    }

    @Override
    public String toString() {
        return nome;
    }
}
