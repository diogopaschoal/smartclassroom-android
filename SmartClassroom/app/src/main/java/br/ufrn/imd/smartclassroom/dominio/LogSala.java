package br.ufrn.imd.smartclassroom.dominio;

import java.io.Serializable;

/**
 * Created by Francisco on 17/11/2015.
 */

public class LogSala implements Serializable{

    private int id;

    private PerfilProfessor professorAnterior;

    private PerfilProfessor professorNovo;

    private String dataLog;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public PerfilProfessor getProfessorAnterior() {
        return professorAnterior;
    }

    public void setProfessorAnterior(PerfilProfessor professorAnterior) {
        this.professorAnterior = professorAnterior;
    }

    public PerfilProfessor getProfessorNovo() {
        return professorNovo;
    }

    public void setProfessorNovo(PerfilProfessor professorNovo) {
        this.professorNovo = professorNovo;
    }

    public String getDataLog() {
        return dataLog;
    }

    public void setDataLog(String dataLog) {
        this.dataLog = dataLog;
    }
}

