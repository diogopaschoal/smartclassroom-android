package br.ufrn.imd.smartclassroom;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.QuickContactBadge;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.ufrn.imd.smartclassroom.dominio.LogSala;
import br.ufrn.imd.smartclassroom.dominio.PerfilProfessor;
import br.ufrn.imd.smartclassroom.utils.JSONProcessor;

public class MainActivity extends ActionBarActivity {

    private static final String PERFIS = "/perfis";
    private static final String POST = "/post";
    private static final String ULTIMO_UPDATE = "/ultimoupdate";
    private Spinner spinner;
    private TextView arcondicionado;
    private TextView computador;
    private TextView projetor;
    private List<PerfilProfessor> professores;
    private LogSala logSalaAtual;

    private BroadcastReceiver receiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {

                professores = (List<PerfilProfessor>) bundle.getSerializable(SmartClassroomService.LISTPROFESSORES);
                logSalaAtual = (LogSala) bundle.getSerializable(SmartClassroomService.SALALOG);
                int resultCode = bundle.getInt(SmartClassroomService.RESULT);
                if (resultCode == RESULT_OK) {
                    montarListaProfessores();
                    updateSpinner();
                } else {
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        requestGetPerfis();
        spinnerProfessoresListener();
    }

    @Override
    protected void onStart() {
        super.onStart();
        registerReceiver(receiver, new IntentFilter(SmartClassroomService.NOTIFICATION));
        Intent intent = new Intent(this, SmartClassroomService.class);
        startService(intent);
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }

    private void init() {
        professores = new ArrayList<PerfilProfessor>();
        spinner = (Spinner) findViewById(R.id.spinner_id);
        arcondicionado = (TextView) findViewById(R.id.arcondicionado_id);
        logSalaAtual = new LogSala();
        computador = (TextView) findViewById(R.id.computador_id);
        projetor = (TextView) findViewById(R.id.projetor_id);
        computador.setText(getResources().getString(R.string.off));
        arcondicionado.setText(getResources().getString(R.string.off));
        projetor.setText(getResources().getString(R.string.off));
    }

    private void requestGetPerfis() {
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = getResources().getString(R.string.base_url) + PERFIS;
        Log.d("GET::LOGSALA", url);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        Log.d("REQUEST", response);
                        try {
                            professores = JSONProcessor.toList(response, PerfilProfessor.class);
                            montarListaProfessores();
                            requestGetLogSala();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("REQ::ERROR::GET_PROF", error.toString());
            }
        });
        // Add the requestGet to the RequestQueue.
        queue.add(stringRequest);
    }

    private void requestGetLogSala() {
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = getResources().getString(R.string.base_url) + ULTIMO_UPDATE;
        Log.d("GET::LOGSALA", url);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        Log.d("REQUEST", response);
                        try {
                            logSalaAtual = JSONProcessor.toObject(response, LogSala.class);
                            updateSpinner();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("REQ::ERROR::GET_LOG", error.toString());
            }
        });
        // Add the requestGet to the RequestQueue.
        queue.add(stringRequest);
    }

    private void resquestPost() {
        final RequestQueue queue = Volley.newRequestQueue(this);
        String json = JSONProcessor.toJSON(logSalaAtual);
        String url = getResources().getString(R.string.base_url) + POST;
        Log.d("POST::JSON", json);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(
                Request.Method.POST, url, json,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // Display the first 500 characters of the response string.
                        Log.d("REQUEST::POST", response.toString());
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("REQ::ERROR::POST", error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };
        // Add the requestGet to the RequestQueue.
        queue.add(jsonObjReq);
    }

    private boolean montarNovoLogSala(PerfilProfessor novoProfessor) {
        if (novoProfessor != null && logSalaAtual != null && logSalaAtual.getProfessorNovo() != null) {
            if (logSalaAtual.getProfessorNovo().getId() != novoProfessor.getId()) {
                PerfilProfessor professorAnterior = logSalaAtual.getProfessorNovo();
                logSalaAtual = new LogSala();
                logSalaAtual.setProfessorAnterior(professorAnterior);
                logSalaAtual.setProfessorNovo(novoProfessor);
                return true;
            }
        }
        return false;
    }

    private void spinnerProfessoresListener() {
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                PerfilProfessor prof = professores.get(position);
                arcondicionado.setText(prof.getTemperatura());
                projetor.setText(prof.getSlides());
                computador.setText(getResources().getString(R.string.on));
                if (montarNovoLogSala(prof)) {
                    resquestPost();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // Do nothing
            }
        });
    }

    private void montarListaProfessores() {
        ArrayAdapter<PerfilProfessor> adapter = new ArrayAdapter<PerfilProfessor>(this, android.R.layout.simple_list_item_1, professores);
        adapter.notifyDataSetChanged();
        spinner.setAdapter(adapter);
    }

    private void updateSpinner() {
        PerfilProfessor professor = logSalaAtual.getProfessorNovo();
        int indexProfessor = getIndex(professor);
        spinner.setSelection(indexProfessor);
        arcondicionado.setText(professor.getNome().equalsIgnoreCase("Nenhum") ? getResources().getString(R.string.off) : professor.getTemperatura());
        projetor.setText(professor.getNome().equalsIgnoreCase("Nenhum") ? getResources().getString(R.string.off) : professor.getSlides());
        computador.setText(professor.getNome().equalsIgnoreCase("Nenhum") ? getResources().getString(R.string.off) : getResources().getString(R.string.on));

    }

    private int getIndex(PerfilProfessor professor) {
        for (int i = 0; i < professores.size(); i++) {
            if (professores.get(i).getId() == professor.getId()) {
                return i;
            }
        }
        return -1;
    }


}
