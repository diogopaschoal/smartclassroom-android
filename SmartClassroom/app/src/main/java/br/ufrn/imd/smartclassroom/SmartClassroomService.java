package br.ufrn.imd.smartclassroom;

import android.app.Activity;
import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.ufrn.imd.smartclassroom.dominio.LogSala;
import br.ufrn.imd.smartclassroom.dominio.PerfilProfessor;
import br.ufrn.imd.smartclassroom.utils.JSONProcessor;

/**
 * Created by Francisco on 22/11/2015.
 */
public class SmartClassroomService extends IntentService {

    private static final String TAG_APP = "SMARTCLASSROOM";
    private static final String PERFIS = "/perfis";
    private static final String ULTIMO_UPDATE = "/ultimoupdate";

    private int result = Activity.RESULT_CANCELED;
    public static final String RESULT = "result";
    public static final String SALALOG = "salalog";
    public static final String LISTPROFESSORES = "listaprofessores";
    public static final String NOTIFICATION = "br.ufrn.imd.smartclassroom.SmartClassroomService";
    private List<PerfilProfessor> professores;
    private LogSala logSalaAtual;

    public SmartClassroomService() {
        super(TAG_APP);
        professores = new ArrayList<PerfilProfessor>();
        logSalaAtual = new LogSala();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        while (true) {
            Log.d("SERVICE::", "SMARTCLASSROOM");
            requestGetPerfis();
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void requestGetPerfis() {
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = getResources().getString(R.string.base_url) + PERFIS;
        Log.d("GET::LOGSALA::SERVICE", url);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        Log.d("REQUEST::SERVICE", response);
                        try {
                            professores = JSONProcessor.toList(response, PerfilProfessor.class);
                            requestGetLogSala();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("REQ::ERROR::GET_PROF", error.toString());
            }
        });
        // Add the requestGet to the RequestQueue.
        queue.add(stringRequest);
    }

    private void requestGetLogSala() {
        RequestQueue queue = Volley.newRequestQueue(this);
        String url = getResources().getString(R.string.base_url) + ULTIMO_UPDATE;
        Log.d("GET::LOGSALA::SERVICE", url);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        Log.d("REQUEST::SERVICE", response);
                        try {
                            logSalaAtual = JSONProcessor.toObject(response, LogSala.class);
                            result = Activity.RESULT_OK;
                            publishResults(result);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("REQ::ERROR::GET_LOG", error.toString());
            }
        });
        // Add the requestGet to the RequestQueue.
        queue.add(stringRequest);
    }

    private void publishResults(int result) {
        Intent intent = new Intent(NOTIFICATION);
        Bundle b = new Bundle();
        b.putSerializable(SALALOG, (Serializable) logSalaAtual);
        b.putSerializable(LISTPROFESSORES, (Serializable) professores);
        intent.putExtras(b);
        intent.putExtra(RESULT, result);
        sendBroadcast(intent);
    }
}
